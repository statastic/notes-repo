FROM node:lts-alpine

RUN mkdir /app
ADD dist /app

ADD node_modules /app/node_modules

WORKDIR /app/

CMD ["node", "main.js"]
